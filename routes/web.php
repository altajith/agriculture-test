<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['namespace' => 'Web'], function () use ($router) {

    $router->get('', [
        'as' => 'dashboard', 'uses' => 'DashboardController@dashboardIndex'
    ]);

});

$router->group(['middleware' => ['guest', 'csrf'], 'namespace' => 'Web'], function () use ($router) {

    $router->get('login', [
        'as' => 'login', 'uses' => 'AuthController@loginIndex'
    ]);
    $router->post('login', [
        'as' => 'login.do', 'uses' => 'AuthController@login'
    ]);

});

$router->group(['middleware' => ['auth', 'csrf'], 'namespace' => 'Web'], function () use ($router) {

    $router->get('crop-types', [
        'as' => 'crop.types', 'uses' => 'CropTypesController@listIndex'
    ]);
    $router->get('crop-type[/{id}]', [
        'as' => 'crop.type', 'uses' => 'CropTypesController@manageIndex'
    ]);
    $router->post('crop-type/create', [
        'as' => 'crop.type.create', 'uses' => 'CropTypesController@create'
    ]);
    $router->post('crop-type/update', [
        'as' => 'crop.type.update', 'uses' => 'CropTypesController@update'
    ]);

    $router->get('fields', [
        'as' => 'fields', 'uses' => 'FieldsController@listIndex'
    ]);
    $router->get('field[/{id}]', [
        'as' => 'field', 'uses' => 'FieldsController@manageIndex'
    ]);
    $router->post('field/create', [
        'as' => 'field.create', 'uses' => 'FieldsController@create'
    ]);
    $router->post('field/update', [
        'as' => 'field.update', 'uses' => 'FieldsController@update'
    ]);

    $router->get('tractors', [
        'as' => 'tractors', 'uses' => 'TractorsController@listIndex'
    ]);
    $router->get('tractor[/{id}]', [
        'as' => 'tractor', 'uses' => 'TractorsController@manageIndex'
    ]);
    $router->post('tractor/create', [
        'as' => 'tractor.create', 'uses' => 'TractorsController@create'
    ]);
    $router->post('tractor/update', [
        'as' => 'tractor.update', 'uses' => 'TractorsController@update'
    ]);

    $router->get('processed-fields', [
        'as' => 'processed.fields', 'uses' => 'ProcessedFieldsController@listIndex'
    ]);
    $router->get('processed-field[/{id}]', [
        'as' => 'processed.field', 'uses' => 'ProcessedFieldsController@manageIndex'
    ]);
    $router->post('processed-field/create', [
        'as' => 'processed.field.create', 'uses' => 'ProcessedFieldsController@create'
    ]);
    $router->post('processed-field/update', [
        'as' => 'processed.field.update', 'uses' => 'ProcessedFieldsController@update'
    ]);
    $router->get('processed-fields/report', [
        'as' => 'processed.field.report', 'uses' => 'ProcessedFieldsController@report'
    ]);
    $router->get('processed-field/delete/{id}', [
        'as' => 'processed.field.delete', 'uses' => 'ProcessedFieldsController@delete'
    ]);

    $router->get('users', [
        'as' => 'users', 'uses' => 'UsersController@listIndex'
    ]);
    $router->get('user[/{id}]', [
        'as' => 'user', 'uses' => 'UsersController@manageIndex'
    ]);
    $router->post('user/save', [
        'as' => 'user.save', 'uses' => 'UsersController@save'
    ]);

    $router->get('logout', [
        'as' => 'logout', 'uses' => 'AuthController@logout'
    ]);

});


$router->group(['prefix' => 'api/v1', 'namespace' => 'Api'], function () use ($router) {

    $router->post('login', [
        'as' => 'api.login', 'uses' => 'ApiTokenController@update'
    ]);

    $router->group(['middleware' => 'api'], function () use ($router) {

        $router->post('crop-type/create', [
            'as' => 'api.crop.type.create', 'uses' => 'CropTypesController@create'
        ]);
        $router->post('crop-type/update', [
            'as' => 'api.crop.type.update', 'uses' => 'CropTypesController@update'
        ]);
        $router->get('crop-type/{id}', [
            'as' => 'api.crop.type.retrieve', 'uses' => 'CropTypesController@retrieve'
        ]);
        $router->get('crop-types', [
            'as' => 'api.crop.type.list', 'uses' => 'CropTypesController@list'
        ]);

        $router->post('field/create', [
            'as' => 'api.field.create', 'uses' => 'FieldsController@create'
        ]);
        $router->post('field/update', [
            'as' => 'api.field.update', 'uses' => 'FieldsController@update'
        ]);
        $router->get('field/{id}', [
            'as' => 'api.field.retrieve', 'uses' => 'FieldsController@retrieve'
        ]);
        $router->get('fields', [
            'as' => 'api.field.list', 'uses' => 'FieldsController@list'
        ]);

        $router->post('tractor/create', [
            'as' => 'api.tractor.create', 'uses' => 'TractorsController@create'
        ]);
        $router->post('tractor/update', [
            'as' => 'api.tractor.update', 'uses' => 'TractorsController@update'
        ]);
        $router->get('tractor/{id}', [
            'as' => 'api.tractor.retrieve', 'uses' => 'TractorsController@retrieve'
        ]);
        $router->get('tractors', [
            'as' => 'api.crop.tractor.list', 'uses' => 'TractorsController@list'
        ]);


        $router->post('processed-field/create', [
            'as' => 'api.processed.field.create', 'uses' => 'ProcessedFieldsController@create'
        ]);
        $router->post('processed-field/update', [
            'as' => 'api.processed.field.update', 'uses' => 'ProcessedFieldsController@update'
        ]);
        $router->get('processed-field/{id}', [
            'as' => 'api.processed.field.retrieve', 'uses' => 'ProcessedFieldsController@retrieve'
        ]);
        $router->get('processed-fields', [
            'as' => 'api.processed.field.list', 'uses' => 'ProcessedFieldsController@list'
        ]);
        $router->get('processed-fields/report', [
            'as' => 'api.processed.field.report', 'uses' => 'ProcessedFieldsController@report'
        ]);
        $router->delete('processed-field/delete/{id}', [
            'as' => 'api.processed.field.delete', 'uses' => 'ProcessedFieldsController@delete'
        ]);
    });
});