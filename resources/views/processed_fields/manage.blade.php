@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <form action="{{ $object ? route('processed.field.update'):route('processed.field.create') }}" method="post">
                <input type="hidden" name="_token" value="{{ $token }}">
                <input type="hidden" name="id" value="{{ $id }}">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tractor_id">Tractor</label>
                            <select required="" class="form-control" name="tractor_id" id="tractor_id">
                                <option value="">Select the tractor</option>
                                @foreach($tractors as $tractor)
                                <option value="{{ $tractor->id }}" {{ $object ? ($object->tractor_id == $tractor->id ? 'selected':''):'' }}>{{ $tractor->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field_id">Field</label>
                            <select required="" class="form-control" name="field_id" id="field_id">
                                <option value="">Select the field</option>
                                @foreach($fields as $field)
                                <option value="{{ $field->id }}" {{ $object ? ($object->field_id == $field->id ? 'selected':''):'' }}>{{ $field->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                @if($user && $user->type == 'admin' && $object)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select required="" class="form-control" name="status" id="status">
                                <option value="pending" {{ $object ? ($object->status == 'pending' ? 'selected':''):'' }}>Pending</option>
                                <option value="approved" {{ $object ? ($object->status == 'approved' ? 'selected':''):'' }}>Approved</option>
                                <option value="denied" {{ $object ? ($object->status == 'denied' ? 'selected':''):'' }}>Denied</option>
                            </select>
                        </div>
                    </div>
                </div>
                @endif

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="the_date">Date</label>
                            <input required="" type="text" class="form-control datepickers" name="the_date" id="the_date" value="{{ $object ? $object->the_date:'' }}" placeholder="Date">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="area_limit">Processed Area</label>
                            <input required="" type="text" class="form-control" name="area_limit" id="area_limit" value="{{ $object ? $object->area_limit:'' }}" placeholder="Processed area">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 button-right">
                        <button type="submit" class="btn btn-success default">Save</button>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
@endsection