@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 button-right nav-bottom">
            <a class="btn btn-secondary default" href="{{ route('processed.field.report') }}">Report</a>
            <a class="btn btn-primary default" href="{{ route('processed.field') }}">Create New Processed Field</a>
        </div>
        <div class="col-md-12">

            @if(count($processed_fields) > 0)
                <table class="table table-striped table-bordered datatables" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Field</th>
                            <th>Tractor</th>
                            <th>Date</th>
                            <th>Processed Area</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Created By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($processed_fields as $processed_field)
                        <tr>
                            <td>{{ $processed_field->id }}</td>
                            <td>{{ $processed_field->field->name }}</td>
                            <td>{{ $processed_field->tractor->name }}</td>
                            <td>{{ $processed_field->the_date }}</td>
                            <td>{{ $processed_field->area_limit }}</td>
                            <td>{{ strtoupper($processed_field->status) }}</td>
                            <td>{{ $processed_field->created_at }}</td>
                            <td>{{ $processed_field->updated_at }}</td>
                            <td>{{ $processed_field->user ? $processed_field->user->name:'' }}</td>
                            <td>
                                <a href="{{ route('processed.field', ['id' => $processed_field->id]) }}">Edit</a> |
                                <a class="delete" href="{{ route('processed.field.delete', ['id' => $processed_field->id]) }}">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $processed_fields->links() }}
            @else
                <h5>Processed fields list is empty!</h5>
            @endif

        </div>
    </div>
</div>
@endsection