@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="row">
    <div class="col-md-12">

        <a data-toggle="collapse" href="#multiFilters" role="button" aria-expanded="false" aria-controls="multiFilters">Advance Search</a>
        <div class="collapse multi-collapse show" id="multiFilters">
            <form action="{{ route('processed.field.report') }}" method="get">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="culture">Name of the Field</label>
                            <input type="text" class="form-control" name="field" id="field" value="{{ !empty($old['field']) ? $old['field']:'' }}" placeholder="Search by the name of the field">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="culture">Name of the Tractor</label>
                            <input type="text" class="form-control" name="tractor" id="tractor" value="{{ !empty($old['tractor']) ? $old['tractor']:'' }}" placeholder="Search by the name of the tractor">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="culture">Culture</label>
                            <input type="text" class="form-control" name="culture" id="culture" value="{{ !empty($old['culture']) ? $old['culture']:'' }}" placeholder="Search by the culture">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input type="text" class="form-control datepickers" name="date" id="date" value="{{ !empty($old['date']) ? $old['date']:'' }}" placeholder="Search by the date">
                        </div>
                    </div>
                    <div class="col-md-1 button-right">
                        <button type="submit" class="btn btn-primary default search-button">Search</button>
                        <a href="{{ route('processed.field.report') }}" class="reset-link">Reset Search</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-12">
        <code>This report contains all the <b>APPROVED</b> processed fields.</code>
        @if(count($processed_fields) > 0)
            <table class="table table-striped table-bordered report-table" style="width:100%">
                <thead>
                    <tr>
                        <th>Name of the Field</th>
                        <th>Culture</th>
                        <th>Date</th>
                        <th>Name of the Tractor</th>
                        <th>Processed Area</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $total_area = 0;
                    @endphp
                    @foreach($processed_fields as $processed_field)
                    <tr>
                        <td>{{ $processed_field->field->name }}</td>
                        <td>{{ $processed_field->field->type->type_name }}</td>
                        <td>{{ $processed_field->the_date }}</td>
                        <td>{{ $processed_field->tractor->name }}</td>
                        <td class="text-right">{{ $processed_field->area_limit }}</td>
                        @php
                            $total_area = $total_area + $processed_field->area_limit;
                        @endphp
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" class="text-right"><b>Total amount of processed area</b></td>
                        <td class="text-right">{{ $total_area }}</td>
                    </tr>
                </tfoot>
            </table>
            {{ $processed_fields->links() }}
        @else
            <h5>No records found!</h5>
        @endif

    </div>
</div>
@endsection