@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 button-right nav-bottom">
            <a class="btn btn-primary default" href="{{ route('tractor') }}">Create New Tractor</a>
        </div>
        <div class="col-md-12">

            @if(count($tractors) > 0)
                <table class="table table-striped table-bordered datatables" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Active</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Created By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tractors as $tractor)
                        <tr>
                            <td>{{ $tractor->id }}</td>
                            <td>{{ $tractor->name }}</td>
                            <td>{{ $tractor->active ? 'Yes':'No' }}</td>
                            <td>{{ $tractor->created_at }}</td>
                            <td>{{ $tractor->updated_at }}</td>
                            <td>{{ $tractor->user ? $tractor->user->name:'' }}</td>
                            <td><a href="{{ route('tractor', ['id' => $tractor->id]) }}">Edit Tractor</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $tractors->links() }}
            @else
                <h5>Tractors list is empty!</h5>
            @endif

        </div>
    </div>
</div>
@endsection