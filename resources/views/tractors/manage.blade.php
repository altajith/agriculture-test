@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <form action="{{ $object ? route('tractor.update'):route('tractor.create') }}" method="post">
                <input type="hidden" name="_token" value="{{ $token }}">
                <input type="hidden" name="id" value="{{ $id }}">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" required="" class="form-control" name="name" id="name" value="{{ $object ? $object->name:'' }}" placeholder="Name of the tractor">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="active">Active</label>
                            <select required="" class="form-control" name="active" id="active">
                                <option value="0" {{ $object ? ($object->active == 0 ? 'selected':''):'' }}>No</option>
                                <option value="1" {{ $object ? ($object->active == 1 ? 'selected':''):'' }}>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 button-right">
                        <button type="submit" class="btn btn-success default">Save</button>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
@endsection