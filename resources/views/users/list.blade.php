@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 button-right nav-bottom">
            <a class="btn btn-primary default" href="{{ route('user') }}">Create New User</a>
        </div>
        <div class="col-md-12">

            @if(count($users) > 0)
                <table class="table table-striped table-bordered datatables" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Type</th>
                            <th>Active</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Created By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ strtoupper($user->type) }}</td>
                            <td>{{ $user->active ? 'Yes':'No' }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>{{ $user->updated_at }}</td>
                            <td>{{ $user->created_user ? $user->created_user->name:'' }}</td>
                            <td><a href="{{ route('user', ['id' => $user->id]) }}">Edit User</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $users->links() }}
            @else
                <h5>Users list is empty!</h5>
            @endif

        </div>
    </div>
</div>
@endsection