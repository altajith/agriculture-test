@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('user.save') }}" method="post">
                <input type="hidden" name="_token" value="{{ $token }}">
                <input type="hidden" name="id" value="{{ $id }}">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" required="" class="form-control" name="name" id="name" value="{{ $object ? $object->name:'' }}" placeholder="Name">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" required="" class="form-control" name="email" id="email" value="{{ $object ? $object->email:'' }}" placeholder="Email">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="type">Type</label>
                            <select required="" class="form-control" name="type" id="type">
                                <option value="user" {{ $object ? ($object->type == 'user' ? 'selected':''):'' }}>User</option>
                                <option value="admin" {{ $object ? ($object->type == 'admin' ? 'selected':''):'' }}>Admin</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="active">Active</label>
                            <select required="" class="form-control" name="active" id="active">
                                <option value="0" {{ $object ? ($object->active == 0 ? 'selected':''):'' }}>No</option>
                                <option value="1" {{ $object ? ($object->active == 1 ? 'selected':''):'' }}>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>

                @if($object)
                <div class="row">
                    <div class="form-group password-change">
                        <label for="change_password">Change the password</label>
                        <input type="checkbox" class="form-control" name="change_password" id="change_password" value="1">
                    </div>
                </div>
                @endif

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" {{ !$object ? 'required=""':'' }} class="form-control" name="password" id="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="c_password">Confirm Password</label>
                            <input type="password" {{ !$object ? 'required=""':'' }} class="form-control" name="c_password" id="c_password" placeholder="Confirm Password">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 button-right">
                        <button type="submit" class="btn btn-success default">Save</button>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
@endsection