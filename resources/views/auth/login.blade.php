@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">

            <h4>@yield('title')</h4>

            <form action="{{ route('login.do') }}" method="post">
                <input type="hidden" name="_token" value="{{ $token }}">


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" required="" class="form-control" name="email" id="email" placeholder="Email">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" required="" class="form-control" name="password" id="password" placeholder="Password">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 button-right">
                        <button type="submit" class="btn btn-success default">Login</button>
                    </div>
                </div>

            </form>

        </div>
        <div class="col-md-3"></div>
    </div>
</div>
@endsection