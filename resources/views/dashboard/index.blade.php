@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h5>Introduction</h5>
            <p>A mini-system what allows storing information about Fields, Tractors and Processed Fields and generating additional reports.</p>
            <ul>
                <li><a href="{{ route('login') }}">Web application</a></li>
                <li><a href="/api/documentation">API Documentation with swagger</a></li>
                <li><a href="{{ route('processed.field.report') }}">Processed field report</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5>Steps to setup</h5>
            <ul>
                <li>
                    Clone the git project from bitbucket <a href="https://bitbucket.org/altajith/agriculture-test/src/master/">https://bitbucket.org/altajith/agriculture-test/src/master/</a>
                    <br/><code>git clone https://bitbucket.org/altajith/agriculture-test.git</code>
                </li>
                <li>
                    Rename the <code>.env.example</code> to <code>.env</code> and setup the database details.
                </li>
                <li>
                    Install Composer in the cloned project
                    <br/><code>composer install</code>
                </li>
                <li>
                    Install NPM in the cloned project
                    <br/><code>npm install</code>
                </li>
                <li>
                    Run db migration to populate the database schema
                    <br/><code>php artisan migrate</code>
                </li>
                <li>
                    Run db seeder to populate the default data
                    <br/><code>php artisan db:seed</code>
                </li>
                <li>
                    Compile the frontend modules
                    <br/><code>npm run dev</code>
                </li>
                <li>
                    To serve the project
                    <br/><code>php -S localhost:8000 -t public</code>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5>Demo</h5>
            <ul>
                <li><a href="http://3.219.92.24">Hosted Location</a></li>
                <li>
                    <div>
                        <strong>Admin login</strong><br/>
                        <span>Email: </span><b>apwajith@gmail.com</b><br/>
                        <span>Password: </span><b>qweasd</b><br/>
                    </div>
                </li>
                <li>
                    <div>
                        <strong>User login</strong><br/>
                        <span>Email: </span><b>ajpraza@gmail.com</b><br/>
                        <span>Password: </span><b>qweasd</b><br/>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5>Hosted Environment</h5>
            <ul>
                <li>IP: <b>3.219.92.24</b></li>
                <li>Platform: <b>AWS EC2 t2.micro (Ubuntu 16.04.1)</b></li>
                <li>PHP: <b>7.2.19</b></li>
                <li>Framework: <b>Lumen</b></li>
                <li>MySql: <b>14.14</b></li>
                <li>Web server: <b>Nginx</b></li>
            </ul>
        </div>
    </div>
</div>
@endsection