@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 button-right nav-bottom">
            <a class="btn btn-primary default" href="{{ route('field') }}">Create New Field</a>
        </div>
        <div class="col-md-12">

            @if(count($fields) > 0)
                <table class="table table-striped table-bordered datatables" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Type of Crops</th>
                            <th>Area</th>
                            <th>Active</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Created By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($fields as $field)
                        <tr>
                            <td>{{ $field->id }}</td>
                            <td>{{ $field->name }}</td>
                            <td>{{ $field->type->type_name }}</td>
                            <td>{{ $field->area }}</td>
                            <td>{{ $field->active ? 'Yes':'No' }}</td>
                            <td>{{ $field->created_at }}</td>
                            <td>{{ $field->updated_at }}</td>
                            <td>{{ $field->user ? $field->user->name:'' }}</td>
                            <td><a href="{{ route('field', ['id' => $field->id]) }}">Edit Field</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $fields->links() }}
            @else
                <h5>Fields list is empty!</h5>
            @endif

        </div>
    </div>
</div>
@endsection