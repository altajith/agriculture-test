@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <form action="{{ $object ? route('field.update'):route('field.create') }}" method="post">
                <input type="hidden" name="_token" value="{{ $token }}">
                <input type="hidden" name="id" value="{{ $id }}">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" required="" class="form-control" name="name" id="name" value="{{ $object ? $object->name:'' }}" placeholder="Name of the field">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="crop_type_id">Type of Crops</label>
                            <select required="" class="form-control" name="crop_type_id" id="crop_type_id">
                                <option value="">Select the type</option>
                                @foreach($types as $type)
                                <option value="{{ $type->id }}" {{ $object ? ($object->crop_type_id == $type->id ? 'selected':''):'' }}>{{ $type->type_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="area">Area</label>
                            <input type="text" required="" class="form-control" name="area" id="area" value="{{ $object ? $object->area:'' }}" placeholder="Field area">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="active">Active</label>
                            <select required="" class="form-control" name="active" id="active">
                                <option value="0" {{ $object ? ($object->active == 0 ? 'selected':''):'' }}>No</option>
                                <option value="1" {{ $object ? ($object->active == 1 ? 'selected':''):'' }}>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 button-right">
                        <button type="submit" class="btn btn-success default">Save</button>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
@endsection