@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 button-right nav-bottom">
            <a class="btn btn-primary default" href="{{ route('crop.type') }}">Create New Crop Type</a>
        </div>
        <div class="col-md-12">

            @if(count($types) > 0)
                <table class="table table-striped table-bordered datatables" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Active</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Created By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($types as $type)
                        <tr>
                            <td>{{ $type->id }}</td>
                            <td>{{ $type->type_name }}</td>
                            <td>{{ $type->active ? 'Yes':'No' }}</td>
                            <td>{{ $type->created_at }}</td>
                            <td>{{ $type->updated_at }}</td>
                            <td>{{ $type->user ? $type->user->name:'' }}</td>
                            <td><a href="{{ route('crop.type', ['id' => $type->id]) }}">Edit Type</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $types->links() }}
            @else
                <h5>Crop types list is empty!</h5>
            @endif

        </div>
    </div>
</div>
@endsection