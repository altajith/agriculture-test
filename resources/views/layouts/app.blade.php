<!DOCTYPE html>
	<head>
		<?php $user = \Illuminate\Support\Facades\Auth::guard('web')->user(); ?>
		<!-- Basic Page Needs
		================================================== -->
		<title>@yield('title')</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!-- CSS
		================================================== -->
		<link rel="stylesheet" href="{{ url('css/mods/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ url('css/mods/bootstrap-datepicker3.min.css') }}" />
		<link rel="stylesheet" href="{{ url('css/mods/dataTables.bootstrap4.min.css') }}" />
		<link rel="stylesheet" href="{{ url('css/mods/datatables.responsive.bootstrap4.min.css') }}" />
		<link rel="stylesheet" href="{{ url('css/app.css') }}" />

	</head>
	<body>

		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="/">Agriculture</a>

			@if($user)
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Crop Types
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="{{ route('crop.type') }}">New</a>
							<a class="dropdown-item" href="{{ route('crop.types') }}">Manage</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Fields
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="{{ route('field') }}">New</a>
							<a class="dropdown-item" href="{{ route('fields') }}">Manage</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Tractors
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="{{ route('tractor') }}">New</a>
							<a class="dropdown-item" href="{{ route('tractors') }}">Manage</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Processed Fields
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="{{ route('processed.field') }}">New</a>
							<a class="dropdown-item" href="{{ route('processed.fields') }}">Manage</a>
						</li>
						@if($user->type == 'admin')
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Users
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="{{ route('user') }}">New</a>
							<a class="dropdown-item" href="{{ route('users') }}">Manage</a>
						</li>
						@endif
						<li class="nav-item">
							<a class="nav-link logout" href="{{ route('logout') }}">Logout</a>
						</li>
					</ul>
				</div>
			@endif
		</nav>

		<div class="container-fluid nav-top">

			@if($user)
				<h4>@yield('title')</h4>

				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						@foreach($breadcrumbs as $breadcrumb)
						<li class="breadcrumb-item {{ $breadcrumb['is_active'] ? 'active':'' }}" aria-current="page">
							<a href="{{ $breadcrumb['url'] }}">{{ $breadcrumb['title'] }}</a>
						</li>
						@endforeach
					</ol>
				</nav>
			@endif

			<div class="container">
				<div class="row">
					<div class="col-md-6">
						@if(isset($success) && !empty($success))
						<div class="alert alert-success" role="alert">{{ $success }}</div>
						@endif
						@if(isset($errors) && count($errors) > 0)
							@foreach($errors as $error)
								<div class="alert alert-danger" role="alert">{{ $error }}</div>
							@endforeach
						@endif
					</div>
				</div>
			</div>

			@yield('content')
		</div>

		<!-- JS
		================================================== -->
		<script src="{{ url('js/mods/jquery-3.3.1.min.js') }}"></script>
		<script src="{{ url('js/mods/bootstrap.bundle.min.js') }}"></script>
		<script src="{{ url('js/mods/bootstrap-datepicker.min.js') }}"></script>
		<script src="{{ url('js/mods/datatables.min.js') }}"></script>
		<script src="{{ url('js/app.js') }}"></script>

		@yield('footer')

	</body>
</html>