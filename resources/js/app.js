$(document).ready(function() {
    $('.datatables').DataTable({
        bPaginate: false,
        responsive: true,
        bInfo: false,
        order: [[ 0, "desc" ]]
    });
    $('.report-table').DataTable({
        bPaginate: false,
        responsive: true,
        bInfo: false,
        bSort: false,
        filter: false
    });
    $('.datepickers').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('.delete').click(function(){
        return confirm("Are you sure you want to delete this entry?");
    });
});