# Built with Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

##Introduction

A mini-system what allows storing information about Fields, Tractors and Processed Fields and generating additional reports.

######Web application login : [click here](http://3.219.92.24/login)
######API Documentation with swagger : [click here](http://3.219.92.24/api/documentation)

##Steps to setup

1. Clone the git project from bitbucket https://bitbucket.org/altajith/agriculture-test/src/master/
    ```git clone https://bitbucket.org/altajith/agriculture-test.git```
2. Rename the **.env.example** to **.env** and setup the database details.
3. Install Composer in the cloned project
    ```composer install```
4. Install NPM in the cloned project
    ```npm install```
5. Run db migration to populate the database schema
    ```php artisan migrate```
6. Run db seeder to populate the default data
    ```php artisan db:seed```
7. Compile the frontend modules
    ```npm run dev```
8. To serve the project
    ```php -S localhost:8000 -t public```

##Demo

######Hosted Location : [click here](http://3.219.92.24/)
####Admin login
    Email: apwajith@gmail.com
    Password: qweasd
####User login
    Email: ajpraza@gmail.com
    Password: qweasd

##Hosted Environment

    IP: 3.219.92.24
    Platform: AWS EC2 t2.micro (Ubuntu 16.04.1)
    PHP: 7.2.19
    Framework: Lumen
    MySql: 14.14
    Web server: Nginx

