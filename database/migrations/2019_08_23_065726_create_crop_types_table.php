<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCropTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crop_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type_name')->unique();
            $table->boolean('active')->default(true);
            $table->bigInteger('created_by');
            $table->timestamps();
            $table->index('id');
            $table->index('type_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crop_types', function (Blueprint $table) {
            $table->dropIndex('id');
            $table->dropIndex('type_name');
        });

        Schema::dropIfExists('crop_types');
    }
}
