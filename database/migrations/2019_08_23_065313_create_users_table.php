<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('type', ['admin', 'user']);
            $table->boolean('active')->default(true);
            $table->bigInteger('created_by');
            $table->timestamps();
            $table->index('id');
            $table->index('name');
            $table->index('email');
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex('id');
            $table->dropIndex('name');
            $table->dropIndex('email');
            $table->dropIndex('created_at');
        });

        Schema::dropIfExists('users');
    }
}
