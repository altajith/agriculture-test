<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('crop_type_id');
            $table->double('area');
            $table->boolean('active')->default(true);
            $table->bigInteger('created_by');
            $table->timestamps();
            $table->index('id');
            $table->index('name');
            $table->index('crop_type_id');
            $table->index('area');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fields', function (Blueprint $table) {
            $table->dropIndex('id');
            $table->dropIndex('name');
            $table->dropIndex('crop_type_id');
            $table->dropIndex('area');
        });

        Schema::dropIfExists('fields');
    }
}
