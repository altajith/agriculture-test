<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessedFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processed_fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tractor_id');
            $table->bigInteger('field_id');
            $table->date('the_date');
            $table->double('area_limit');
            $table->bigInteger('created_by');
            $table->timestamps();
            $table->index('id');
            $table->index('tractor_id');
            $table->index('field_id');
            $table->index('the_date');
            $table->index('area_limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('processed_fields', function (Blueprint $table) {
            $table->dropIndex('id');
            $table->dropIndex('tractor_id');
            $table->dropIndex('field_id');
            $table->dropIndex('the_date');
            $table->dropIndex('area_limit');
        });

        Schema::dropIfExists('processed_fields');
    }
}
