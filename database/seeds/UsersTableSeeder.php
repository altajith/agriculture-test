<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Ajith Wijesekara Admin',
            'email' => 'apwajith@gmail.com',
            'password' => Hash::make('qweasd'),
            'created_by' => 1,
            'type' => 'admin',
            'active' => true,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'name' => 'Ajith Wijesekara User',
            'email' => 'ajpraza@gmail.com',
            'password' => Hash::make('qweasd'),
            'created_by' => 1,
            'type' => 'user',
            'active' => true,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
