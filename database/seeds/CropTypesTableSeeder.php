<?php

use Illuminate\Database\Seeder;

class CropTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('crop_types')->insert([
            'type_name' => 'Wheat',
            'created_by' => 1,
            'active' => true,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('crop_types')->insert([
            'type_name' => 'Broccoli',
            'created_by' => 1,
            'active' => true,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('crop_types')->insert([
            'type_name' => 'Strawberries',
            'created_by' => 1,
            'active' => true,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
