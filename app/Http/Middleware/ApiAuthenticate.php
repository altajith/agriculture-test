<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use App\Models\CropType;
use App\Models\Field;
use App\Models\Tractor;
use App\Models\ProcessedField;

class ApiAuthenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard('api')->guest()) {
            return response('Unauthorized.', 401);
        }

        $user = $this->auth->guard('api')->user();
        if ($user->type == 'admin') {
            return $next($request);
        } else {
            if ($request->is('user') || $request->is('user/*') || $request->is('users') || $request->is('users/*')) {
                return response('Unauthorized.', 401);
            }
            if ($request->isMethod('post')) {
                if ($request->input('id') > 0) {
                    $validated_user = false;
                    if ($request->is('api/v1/crop-type/update')) {
                        $validated_user = CropType::where('id', $request->input('id'))->where('created_by', $user->id)->exists();
                    } elseif ($request->is('api/v1/field/update')) {
                        $validated_user = Field::where('id', $request->input('id'))->where('created_by', $user->id)->exists();
                    }elseif ($request->is('api/v1/tractor/update')) {
                        $validated_user = Tractor::where('id', $request->input('id'))->where('created_by', $user->id)->exists();
                    }elseif ($request->is('api/v1/processed-field/update')) {
                        $validated_user = ProcessedField::where('id', $request->input('id'))->where('created_by', $user->id)->exists();
                    }
                    if (!$validated_user) {
                        return response('Unauthorized.', 401);
                    }
                }
            } elseif ($request->isMethod('delete')) {
                return response('Unauthorized.', 401);
            }
        }

        return $next($request);
    }
}
