<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Dashboard
     *
     * @return View
     */
    public function dashboardIndex(Request $request)
    {
        $title = 'Home';
        $breadcrumbs = [];

        return view('dashboard.index', [
            'title' => $title,
            'token' => $request->session()->token(),
            'breadcrumbs' => $breadcrumbs
        ]);
    }
}
