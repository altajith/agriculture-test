<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CropType;
use App\Helpers\Helper;

class CropTypesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Display the crop types list
     *
     * @param  Request  $request
     *
     * @return View
     */
    public function listIndex(Request $request)
    {
        $title = 'Manage Crop Types';

        $breadcrumbs = [
            [
                'url' => route('crop.types'),
                'title' => $title,
                'is_active' => true
            ]
        ];

        $types = CropType::orderBy('id', 'desc')->paginate(15);


        return view('crop_types.list', [
            'title' => $title,
            'breadcrumbs' => $breadcrumbs,
            'success' =>  Helper::success($request),
            'errors' =>  Helper::errors($request),
            'types' => $types
        ]);
    }

    /**
     * Create/Edit display for a crop type
     *
     * @param  Request  $request
     * @param  integer  $id
     *
     * @return View
     */
    public function manageIndex(Request $request, $id = 0)
    {
        $title = 'Create New Crop Type';
        if ($id > 0) {
            $title = 'Edit #'.$id.' Crop Type';
        }

        $breadcrumbs = [
            [
                'url' => route('crop.types'),
                'title' => 'Manage Crop Types',
                'is_active' => false
            ],
            [
                'url' => ($id > 0 ? route('crop.type', ['id' => $id]):route('crop.type')),
                'title' => $title,
                'is_active' => true
            ]
        ];

        return view('crop_types.manage', [
            'title' => $title,
            'token' => $request->session()->token(),
            'breadcrumbs' => $breadcrumbs,
            'errors' =>  Helper::errors($request),
            'object' => $id > 0 ? CropType::find($id):false,
            'id' => $id
        ]);
    }

    /**
     * Create the crop type
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $errors = [];
        $data = $request->all();
        if (empty($data['type_name'])) {
            $errors[] = 'Please enter the type name.';
        }

        if (count($errors) == 0) {
            if (Helper::createOrUpdateCropType($data, $errors)) {
                $request->session()->put('success', 'Crop type has been successfully created.');
                return redirect()->route('crop.types');
            }
        }

        return Helper::redirect('crop.type', $request, $data, $errors);
    }

    /**
     * Update the crop type
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $errors = [];
        $data = $request->all();
        if (empty($data['type_name'])) {
            $errors[] = 'Please enter the type name.';
        }

        if (count($errors) == 0) {
            if (Helper::createOrUpdateCropType($data, $errors)) {
                $request->session()->put('success', 'Crop type has been successfully updated.');
                return redirect()->route('crop.types');
            }
        }

        return Helper::redirect('crop.type', $request, $data, $errors);
    }
}
