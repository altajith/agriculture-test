<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tractor;
use App\Helpers\Helper;

class TractorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Display the tractors list
     *
     * @param  Request  $request
     *
     * @return View
     */
    public function listIndex(Request $request)
    {
        $title = 'Manage Tractors';

        $breadcrumbs = [
            [
                'url' => route('tractors'),
                'title' => $title,
                'is_active' => true
            ]
        ];

        $tractors = Tractor::orderBy('id', 'desc')->paginate(15);

        return view('tractors.list', [
            'title' => $title,
            'breadcrumbs' => $breadcrumbs,
            'success' =>  Helper::success($request),
            'errors' =>  Helper::errors($request),
            'tractors' => $tractors
        ]);
    }

    /**
     * Create/Edit display for a tractor
     *
     * @param  Request  $request
     * @param  integer  $id
     *
     * @return View
     */
    public function manageIndex(Request $request, $id = 0)
    {
        $title = 'Create New Tractor';
        if ($id > 0) {
            $title = 'Edit #'.$id.' Tractor';
        }

        $breadcrumbs = [
            [
                'url' => route('tractors'),
                'title' => 'Manage Tractors',
                'is_active' => false
            ],
            [
                'url' => ($id > 0 ? route('tractor', ['id' => $id]):route('tractor')),
                'title' => $title,
                'is_active' => true
            ]
        ];

        return view('tractors.manage', [
            'title' => $title,
            'token' => $request->session()->token(),
            'breadcrumbs' => $breadcrumbs,
            'errors' =>  Helper::errors($request),
            'object' => $id > 0 ? Tractor::find($id):false,
            'id' => $id
        ]);
    }

    /**
     * Create the tractor
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $errors = [];
        $data = $request->all();
        if (empty($data['name'])) {
            $errors[] = 'Please enter the tractor name.';
        }

        if (count($errors) == 0) {
            if (Helper::createOrUpdateTractor($data, $errors)) {
                $request->session()->put('success', 'Tractor has been successfully created.');
                return redirect()->route('tractors');
            }
        }

        return Helper::redirect('tractor', $request, $data, $errors);
    }

    /**
     * Update the tractor
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $errors = [];
        $data = $request->all();
        if (empty($data['name'])) {
            $errors[] = 'Please enter the tractor name.';
        }

        if (count($errors) == 0) {
            if (Helper::createOrUpdateTractor($data, $errors)) {
                $request->session()->put('success', 'Tractor has been successfully updated.');
                return redirect()->route('tractors');
            }
        }

        return Helper::redirect('tractor', $request, $data, $errors);
    }
}
