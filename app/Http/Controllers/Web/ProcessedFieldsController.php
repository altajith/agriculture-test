<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Field;
use App\Models\Tractor;
use App\Models\ProcessedField;
use App\Helpers\Helper;

class ProcessedFieldsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Display the processed fields list
     *
     * @param  Request  $request
     *
     * @return View
     */
    public function listIndex(Request $request)
    {
        $title = 'Manage Processed Fields';

        $breadcrumbs = [
            [
                'url' => route('processed.fields'),
                'title' => $title,
                'is_active' => true
            ]
        ];

        $processed_fields = ProcessedField::orderBy('id', 'desc')->paginate(15);

        return view('processed_fields.list', [
            'title' => $title,
            'breadcrumbs' => $breadcrumbs,
            'success' =>  Helper::success($request),
            'errors' =>  Helper::errors($request),
            'processed_fields' => $processed_fields
        ]);
    }

    /**
     * Create/Edit display for a processed field
     *
     * @param  Request  $request
     * @param  string  $id
     *
     * @return View
     */
    public function manageIndex(Request $request, $id = 0)
    {
        $title = 'Create New Processed Field';
        if ($id > 0) {
            $title = 'Edit #'.$id.' Processed Field';
        }

        $breadcrumbs = [
            [
                'url' => route('processed.fields'),
                'title' => 'Manage Processed Fields',
                'is_active' => false
            ],
            [
                'url' => ($id > 0 ? route('processed.field', ['id' => $id]):route('processed.field')),
                'title' => $title,
                'is_active' => true
            ]
        ];

        $fields = Field::where('active', true)->orderBy('name', 'asc')->get();
        $tractors = Tractor::where('active', true)->orderBy('name', 'asc')->get();
        $auth_user = Helper::web_user();

        return view('processed_fields.manage', [
            'title' => $title,
            'token' => $request->session()->token(),
            'breadcrumbs' => $breadcrumbs,
            'errors' =>  Helper::errors($request),
            'fields' => $fields,
            'tractors' => $tractors,
            'object' => $id > 0 ? ProcessedField::find($id):false,
            'id' => $id,
            'user' => $auth_user
        ]);
    }

    /**
     * Create the field
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $errors = [];
        $data = $request->all();
        if (empty($data['tractor_id'])) {
            $errors[] = 'Please enter the tractor.';
        }
        if (empty($data['field_id'])) {
            $errors[] = 'Please select the field.';
        }
        if (empty($data['the_date'])) {
            $errors[] = 'Please enter the date.';
        }
        if (empty($data['area_limit'])) {
            $errors[] = 'Please enter the processed area.';
        }
        if (!is_numeric($data['area_limit'])) {
            $errors[] = 'Processed area should be a numeric value.';
        }

        if (!empty($data['field_id']) && !empty($data['area_limit'])) {
            $field = Field::find($data['field_id']);
            if ($field) {
                if ($data['area_limit'] > $field->area) {
                    $errors[] = 'Processed area cannot be exceeded '.$field->area.'.';
                }
            } else {
                $errors[] = 'Please select a valid field.';
            }
        }

        if (count($errors) === 0) {
            if (Helper::createOrUpdateProcessedField($data, $errors)) {
                $request->session()->put('success', 'Processed field has been successfully created.');
                return redirect()->route('processed.fields');
            }
        }

        return Helper::redirect('processed.field', $request, $data, $errors);
    }

    /**
     * Update the field
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $errors = [];
        $data = $request->all();
        if (empty($data['tractor_id'])) {
            $errors[] = 'Please enter the tractor.';
        }
        if (empty($data['field_id'])) {
            $errors[] = 'Please select the field.';
        }
        if (empty($data['the_date'])) {
            $errors[] = 'Please enter the date.';
        }
        if (empty($data['area_limit'])) {
            $errors[] = 'Please enter the processed area.';
        }
        if (!is_numeric($data['area_limit'])) {
            $errors[] = 'Processed area should be a numeric value.';
        }

        if (!empty($data['field_id']) && !empty($data['area_limit'])) {
            $field = Field::find($data['field_id']);
            if ($field) {
                if ($data['area_limit'] > $field->area) {
                    $errors[] = 'Processed area cannot be exceeded '.$field->area.'.';
                }
            } else {
                $errors[] = 'Please select a valid field.';
            }
        }

        if (count($errors) === 0) {
            if (Helper::createOrUpdateProcessedField($data, $errors)) {
                $request->session()->put('success', 'Processed field has been successfully updated.');
                return redirect()->route('processed.fields');
            }
        }

        return Helper::redirect('processed.field', $request, $data, $errors);
    }

    /**
     * Report for the processed fields
     *
     * @param  Request  $request
     *
     * @return View
     */
    public function report(Request $request)
    {
        $title = 'Processed Fields Report';

        $breadcrumbs = [
            [
                'url' => route('processed.fields'),
                'title' => 'Manage Processed Fields',
                'is_active' => false
            ],
            [
                'url' => route('processed.field.report'),
                'title' => $title,
                'is_active' => true
            ]
        ];

        $data = $request->all();
        $processed_fields = Helper::process_fields_report($data);

        return view('processed_fields.report', [
            'title' => $title,
            'breadcrumbs' => $breadcrumbs,
            'success' =>  Helper::success($request),
            'errors' =>  Helper::errors($request),
            'processed_fields' => $processed_fields,
            'old' => $data
        ]);
    }

    /**
     * Delete for the processed field
     *
     * @param  Request  $request
     *
     * @return View
     */
    public function delete(Request $request, $id)
    {
        $data = [];
        $field = ProcessedField::where('id', $id)->first();
        if ($field) {
            $field->delete();
            $request->session()->put('success', 'Processed field has been successfully deleted.');
            return redirect()->route('processed.fields');
        } else {
            $errors[] = "Invalid processed field.";
        }
        return Helper::redirect('processed.fields', $request, $data, $errors);
    }
}
