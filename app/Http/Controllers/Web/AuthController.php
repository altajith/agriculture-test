<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Display the login page
     *
     * @param  Request  $request
     *
     * @return View
     */
    public function loginIndex(Request $request)
    {
        $title = 'Login';

        $breadcrumbs = [];

        return view('auth.login', [
            'title' => $title,
            'breadcrumbs' => $breadcrumbs,
            'token' => $request->session()->token(),
            'success' =>  Helper::success($request),
            'errors' =>  Helper::errors($request)
        ]);
    }

    /**
     * Post the login
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function login(Request $request)
    {
        $errors = [];
        $data = $request->all();
        if (empty($data['email'])) {
            $errors[] = 'Please enter the email.';
        }

        if (empty($data['password'])) {
            $errors[] = 'Please enter the password.';
        }

        if (count($errors) === 0) {
            $user = User::where('email', $data['email'])->where('active', true)->first();
            if ($user) {
                if(Auth::guard('web')->attempt(['email' => $data['email'], 'password' => $data['password']])){
                    Auth::guard('web')->login($user);
                    $request->session()->put('success', 'You have successfully logged in.');
                    return redirect()->route('dashboard');
                } else {
                    $errors[] = 'Password is not correct, please try again.';
                }
            } else {
                $errors[] = 'System cannot find the email address, please try again.';
            }
        }

        return Helper::redirect('login', $request, $data, $errors);
    }

    /**
     * Logout from the system
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function logout(Request $request)
    {
        Auth::guard('web')->logout();
        return redirect()->route('login');
    }
}
