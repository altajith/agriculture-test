<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CropType;
use App\Models\Field;
use App\Helpers\Helper;

class FieldsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Display the fields list
     *
     * @param  Request  $request
     *
     * @return View
     */
    public function listIndex(Request $request)
    {
        $title = 'Manage Fields';

        $breadcrumbs = [
            [
                'url' => route('fields'),
                'title' => $title,
                'is_active' => true
            ]
        ];

        $fields = Field::orderBy('id', 'desc')->paginate(15);

        return view('fields.list', [
            'title' => $title,
            'breadcrumbs' => $breadcrumbs,
            'success' =>  Helper::success($request),
            'errors' =>  Helper::errors($request),
            'fields' => $fields
        ]);
    }

    /**
     * Create/Edit display for a field
     *
     * @param  Request  $request
     * @param  string  $id
     *
     * @return View
     */
    public function manageIndex(Request $request, $id = 0)
    {
        $title = 'Create New Field';
        if ($id > 0) {
            $title = 'Edit #'.$id.' Field';
        }

        $breadcrumbs = [
            [
                'url' => route('fields'),
                'title' => 'Manage Fields',
                'is_active' => false
            ],
            [
                'url' => ($id > 0 ? route('field', ['id' => $id]):route('field')),
                'title' => $title,
                'is_active' => true
            ]
        ];

        $types = CropType::where('active', true)->orderBy('type_name', 'asc')->get();

        return view('fields.manage', [
            'title' => $title,
            'token' => $request->session()->token(),
            'breadcrumbs' => $breadcrumbs,
            'errors' =>  Helper::errors($request),
            'types' => $types,
            'object' => $id > 0 ? Field::find($id):false,
            'id' => $id
        ]);
    }

    /**
     * Create the field
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $errors = [];
        $data = $request->all();
        if (empty($data['name'])) {
            $errors[] = 'Please enter the field name.';
        }
        if (empty($data['crop_type_id'])) {
            $errors[] = 'Please select the type of crops.';
        }
        if (empty($data['area'])) {
            $errors[] = 'Please enter the field area.';
        }
        if (!is_numeric($data['area'])) {
            $errors[] = 'Field area should be a numeric value.';
        }

        if (count($errors) == 0) {
            if (Helper::createOrUpdateField($data, $errors)) {
                $request->session()->put('success', 'Field has been successfully created.');
                return redirect()->route('fields');
            }
        }

        return Helper::redirect('field', $request, $data, $errors);
    }

    /**
     * Update the field
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $errors = [];
        $data = $request->all();
        if (empty($data['name'])) {
            $errors[] = 'Please enter the field name.';
        }
        if (empty($data['crop_type_id'])) {
            $errors[] = 'Please select the type of crops.';
        }
        if (empty($data['area'])) {
            $errors[] = 'Please enter the field area.';
        }
        if (!is_numeric($data['area'])) {
            $errors[] = 'Field area should be a numeric value.';
        }

        if (count($errors) == 0) {
            if (Helper::createOrUpdateField($data, $errors)) {
                $request->session()->put('success', 'Field has been successfully updated.');
                return redirect()->route('fields');
            }
        }

        return Helper::redirect('field', $request, $data, $errors);
    }
}
