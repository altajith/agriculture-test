<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Display the users list
     *
     * @param  Request  $request
     *
     * @return View
     */
    public function listIndex(Request $request)
    {
        $title = 'Manage Users';

        $breadcrumbs = [
            [
                'url' => route('users'),
                'title' => $title,
                'is_active' => true
            ]
        ];

        $users = User::orderBy('id', 'desc')->paginate(15);


        return view('users.list', [
            'title' => $title,
            'breadcrumbs' => $breadcrumbs,
            'success' =>  Helper::success($request),
            'errors' =>  Helper::errors($request),
            'users' => $users
        ]);
    }

    /**
     * Create/Edit display for a user
     *
     * @param  Request  $request
     * @param  integer  $id
     *
     * @return View
     */
    public function manageIndex(Request $request, $id = 0)
    {
        $title = 'Create New User';
        if ($id > 0) {
            $title = 'Edit #'.$id.' User';
        }

        $breadcrumbs = [
            [
                'url' => route('users'),
                'title' => 'Manage Users',
                'is_active' => false
            ],
            [
                'url' => ($id > 0 ? route('user', ['id' => $id]):route('user')),
                'title' => $title,
                'is_active' => true
            ]
        ];

        return view('users.manage', [
            'title' => $title,
            'token' => $request->session()->token(),
            'breadcrumbs' => $breadcrumbs,
            'errors' =>  Helper::errors($request),
            'object' => $id > 0 ? User::find($id):false,
            'id' => $id
        ]);
    }

    /**
     * Save the user
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function save(Request $request)
    {
        $errors = [];
        $data = $request->all();
        if (empty($data['name'])) {
            $errors[] = 'Please enter the user name.';
        }

        if (empty($data['email'])) {
            $errors[] = 'Please enter the user email.';
        } else {
            if (array_key_exists('id', $data) && $data['id'] > 0) {
                if(User::where('id', '!=', $data['id'])->where('email', $data['email'])->exists()) {
                    $errors[] = '"'.$data['email'].'" is already in the system, please enter a different user email.';
                }
            } else {
                if(User::where('email', $data['email'])->exists()) {
                    $errors[] = '"'.$data['email'].'" is already in the system, please enter a different user email.';
                }
            }
        }

        if (empty($data['type'])) {
            $errors[] = 'Please select the user type.';
        }

        $password_change = false;
        if ($data['id'] == 0 || (!empty($data['change_password']) && $data['change_password'] == 1)) {
            $password_change = true;
        }

        if ($password_change) {
            if (empty($data['password'])) {
                $errors[] = 'Please enter the user password.';
            }

            if (empty($data['c_password'])) {
                $errors[] = 'Please enter the user confirmation password.';
            }

            if ($data['password'] != $data['c_password']) {
                $errors[] = 'User password and the confirmation password has to be the same.';
            }
        }

        if (count($errors) === 0) {
            if (array_key_exists('id', $data) && $data['id'] > 0) {
                $user = User::find($data['id']);
                if (!empty($data['change_password']) && $data['change_password'] == 1) {
                    $user->password = Hash::make($data['password']);
                }
            } else {
                $user = new User();
                $auth_user = Helper::web_user();
                $user->created_by = $auth_user ? $auth_user->id:0;
                $user->password = Hash::make($data['password']);
            }
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->type = $data['type'];
            $user->active = $data['active'] == 1 ? true:false;
            if ($user->save()) {
                $request->session()->put('success', 'User has been successfully saved.');
                return redirect()->route('users');
            }
        }

        return Helper::redirect('user', $request, $data, $errors);
    }
}
