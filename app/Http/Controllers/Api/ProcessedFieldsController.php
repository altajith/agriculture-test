<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\ProcessedField;
use App\Models\Field;
use App\Helpers\Helper;

class ProcessedFieldsController extends Controller
{

    /**
     * @OA\Post(
     *   path="/api/v1/processed-field/create",
     *   tags={"Processed Fields"},
     *   summary="Create a processed field in the site with form data",
     *   description="",
     *   operationId="processedFieldCreate",
     *   @OA\RequestBody(
     *       required=false,
     *       @OA\MediaType(
     *           mediaType="application/x-www-form-urlencoded",
     *           @OA\Schema(
     *               type="object",
     *               required={"tractor_id","field_id","the_date","area_limit"},
     *               @OA\Property(
     *                   property="tractor_id",
     *                   description="The tractor id for the processed field",
     *                   type="integer"
     *               ),
     *               @OA\Property(
     *                   property="field_id",
     *                   description="The field id for the processed field",
     *                   type="integer"
     *               ),
     *               @OA\Property(
     *                   property="the_date",
     *                   description="Date for the processed field",
     *                   type="date"
     *               ),
     *               @OA\Property(
     *                   property="area_limit",
     *                   description="Area for the processed field",
     *                   type="double"
     *               ),
     *           )
     *       )
     *   ),
     *   @OA\Response(
     *       response="200",
     *       description="Returns the created processed field object with the status or errors.",
     *       @OA\JsonContent(ref="#/components/schemas/ProcessedField")
     *   ),
     *   @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *   ),
     *   security={
     *         {"Authentication": {}}
     *   }
     * )
     */
    public function create(Request $request)
    {
        $errors = [];
        $success = false;
        $object = null;

        $data = $request->all();

        if (empty($data['tractor_id'])) {
            $errors[] = 'Please enter the tractor id.';
        }

        if (empty($data['field_id'])) {
            $errors[] = 'Please enter the field id.';
        }

        if (empty($data['the_date'])) {
            $errors[] = 'Please enter the date.';
        }

        if (empty($data['area_limit'])) {
            $errors[] = 'Please enter the processed area.';
        }
        if (!is_numeric($data['area_limit'])) {
            $errors[] = 'Processed area should be a numeric value.';
        }

        if (!empty($data['field_id']) && !empty($data['area_limit'])) {
            $field = Field::find($data['field_id']);
            if ($field) {
                if ($data['area_limit'] > $field->area) {
                    $errors[] = 'Processed area cannot be exceeded '.$field->area.'.';
                }
            } else {
                $errors[] = 'Please enter a valid field id.';
            }
        }

        if (count($errors) == 0) {
            if ($object = Helper::createOrUpdateProcessedField($data, $errors)) {
                $success = true;
            }
        }

        return response()->json(["success" => $success, "object" => $object, "errors" => $errors], Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *   path="/api/v1/processed-field/update",
     *   tags={"Processed Fields"},
     *   summary="Update a processed field in the site with form data",
     *   description="",
     *   operationId="processedFieldUpdate",
     *   @OA\RequestBody(
     *       required=false,
     *       @OA\MediaType(
     *           mediaType="application/x-www-form-urlencoded",
     *           @OA\Schema(
     *               type="object",
     *               required={"id"},
     *               @OA\Property(
     *                   property="id",
     *                   description="The id for the processed field",
     *                   type="integer",
     *                   minimum=1
     *               ),
     *               @OA\Property(
     *                   property="tractor_id",
     *                   description="The tractor id for the processed field",
     *                   type="integer"
     *               ),
     *               @OA\Property(
     *                   property="field_id",
     *                   description="The field id for the processed field",
     *                   type="integer"
     *               ),
     *               @OA\Property(
     *                   property="the_date",
     *                   description="Date for the processed field",
     *                   type="date"
     *               ),
     *               @OA\Property(
     *                   property="area_limit",
     *                   description="Area for the processed field",
     *                   type="double"
     *               ),
     *               @OA\Property(
     *                   property="status",
     *                   description="Status of the processed field (Only admin users can change this)",
     *                   type="enum",
     *                   enum={"pending", "approved", "denied"}
     *               ),
     *           )
     *       )
     *   ),
     *   @OA\Response(
     *       response="200",
     *       description="Returns the created processed field object with the status or errors.",
     *       @OA\JsonContent(ref="#/components/schemas/ProcessedField")
     *   ),
     *   @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *   ),
     *   security={
     *         {"Authentication": {}}
     *   }
     * )
     */
    public function update(Request $request)
    {
        $errors = [];
        $success = false;
        $object = null;

        $data = $request->all();
        if (empty($data['id'])) {
            $errors[] = 'Please enter the processed field id.';
        }

        if (!empty($data['area_limit'])) {
            if (!is_numeric($data['area_limit'])) {
                $errors[] = 'Processed area should be a numeric value.';
            }
        }

        if (!empty($data['field_id']) && !empty($data['area_limit'])) {
            $field = Field::find($data['field_id']);
            if ($field) {
                if ($data['area_limit'] > $field->area) {
                    $errors[] = 'Processed area cannot be exceeded '.$field->area.'.';
                }
            } else {
                $errors[] = 'Please enter a valid field id.';
            }
        }

        if (count($errors) == 0) {
            if ($object = Helper::createOrUpdateProcessedField($data, $errors)) {
                $success = true;
            }
        }

        return response()->json(["success" => $success, "object" => $object, "errors" => $errors], Response::HTTP_OK);
    }

    /**
     * @OA\Get(
     *   path="/api/v1/processed-field/{id}",
     *   tags={"Processed Fields"},
     *   summary="Get a tractor in the site with parameters",
     *   description="",
     *   operationId="processedFieldRetrieve",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of processed field that needs to be fetched",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             minimum=1
     *         )
     *     ),
     *     @OA\Response(
     *       response="200",
     *       description="Returns the processed field object with the status or errors.",
     *       @OA\JsonContent(ref="#/components/schemas/ProcessedField")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Processed field not found"
     *     ),
     *     @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *     ),
     *     security={
     *         {"Authentication": {}}
     *     }
     * )
     */
    public function retrieve(Request $request, $id)
    {
        $errors = [];
        $success = false;
        $object = null;

        $object = ProcessedField::find($id);
        if ($object) {
            $success = true;
        } else {
            $errors[] = 'Invalid processed field id.';
        }

        return response()->json(["success" => $success, "object" => $object, "errors" => $errors], Response::HTTP_OK);
    }

    /**
     * @OA\Get(
     *   path="/api/v1/processed-fields",
     *   tags={"Processed Fields"},
     *   summary="Get processed fields in the site with parameters",
     *   description="",
     *   operationId="processedFieldList",
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Current page",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Page limit (max=100)",
     *         @OA\Schema(type="integer",example=25,maximum=100)
     *     ),
     *     @OA\Response(
     *       response="200",
     *       description="Returns the processed field objects with the status or errors.",
     *       @OA\JsonContent(
     *          @OA\Property(property="success",type="boolean",example=true),
     *          @OA\Property(
     *               property="objects",
     *               type="object",
     *               @OA\Property(property="current_page",type="integer",example=1),
     *               @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                     type="object",
     *                      @OA\Property(property="id",type="integer",example=1),
     *                      @OA\Property(property="tractor_id",type="integer",example=1),
     *                      @OA\Property(property="field_id",type="integer",example=1),
     *                      @OA\Property(property="the_date",type="date",example="2019-10-20"),
     *                      @OA\Property(property="area_limit",type="double",example=100),
     *                      @OA\Property(property="status",type="string",example="pending"),
     *                      @OA\Property(property="created_by",type="integer",example=1),
     *                      @OA\Property(property="created_at",type="datetime",example="2019-08-24 15:23:16"),
     *                      @OA\Property(property="updated_at",type="datetime",example="2019-08-24 15:23:16"),
     *                  )
     *              ),
     *              @OA\Property(property="first_page_url",type="string",example="/api/v1/processed-fields?page=1"),
     *              @OA\Property(property="from",type="integer",example=1),
     *              @OA\Property(property="last_page",type="integer",example=1),
     *              @OA\Property(property="last_page_url",type="string",example="/api/v1/processed-fields?page=2"),
     *              @OA\Property(property="next_page_url",type="string",example="/api/v1/processed-fields?page=3"),
     *              @OA\Property(property="path",type="string",example="/api/v1/processed-fields"),
     *              @OA\Property(property="per_page",type="integer",example=25),
     *              @OA\Property(property="prev_page_url",type="string",example="/api/v1/processed-fields?page=1"),
     *              @OA\Property(property="to",type="integer",example=9),
     *              @OA\Property(property="total",type="integer",example=9)
     *          ),
     *          @OA\Property(property="errors",type="array",@OA\Items())
     *       )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Processed fields not found"
     *     ),
     *     @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *     ),
     *     security={
     *         {"Authentication": {}}
     *     }
     * )
     */
    public function list(Request $request)
    {
        $errors = [];
        $success = true;
        $object = null;

        $data = $request->all();

        $objects = ProcessedField::where('id', '>', 0);
        $objects = Helper::api_filter($data, $objects);

        return response()->json(["success" => $success, "objects" => $objects, "errors" => $errors], Response::HTTP_OK);
    }

    /**
     * @OA\Get(
     *   path="/api/v1/processed-fields/report",
     *   tags={"Processed Fields"},
     *   summary="Report for processed fields",
     *   description="",
     *   operationId="processedFieldReport",
     *     @OA\Parameter(
     *         name="field",
     *         in="query",
     *         description="Search the processed fields by the field name",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="tractor",
     *         in="query",
     *         description="Search the processed fields by the tractor name",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="date",
     *         in="query",
     *         description="Search the processed fields by the date",
     *         @OA\Schema(type="date")
     *     ),
     *     @OA\Parameter(
     *         name="culture",
     *         in="query",
     *         description="Search the processed fields by the culture",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Current page",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Page limit (max=100)",
     *         @OA\Schema(type="integer",example=25,maximum=100)
     *     ),
     *     @OA\Response(
     *       response="200",
     *       description="Returns the processed field objects with the status or errors.",
     *       @OA\JsonContent(
     *          @OA\Property(property="success",type="boolean",example=true),
     *          @OA\Property(
     *               property="objects",
     *               type="object",
     *               @OA\Property(property="current_page",type="integer",example=1),
     *               @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                     type="object",
     *                      @OA\Property(property="id",type="integer",example=1),
     *                      @OA\Property(property="tractor_id",type="integer",example=1),
     *                      @OA\Property(property="field_id",type="integer",example=1),
     *                      @OA\Property(property="the_date",type="date",example="2019-10-20"),
     *                      @OA\Property(property="area_limit",type="double",example=100),
     *                      @OA\Property(property="status",type="string",example="pending"),
     *                      @OA\Property(property="created_by",type="integer",example=1),
     *                      @OA\Property(property="created_at",type="datetime",example="2019-08-24 15:23:16"),
     *                      @OA\Property(property="updated_at",type="datetime",example="2019-08-24 15:23:16"),
     *                  )
     *              ),
     *              @OA\Property(property="first_page_url",type="string",example="/api/v1/processed-fields?page=1"),
     *              @OA\Property(property="from",type="integer",example=1),
     *              @OA\Property(property="last_page",type="integer",example=1),
     *              @OA\Property(property="last_page_url",type="string",example="/api/v1/processed-fields?page=2"),
     *              @OA\Property(property="next_page_url",type="string",example="/api/v1/processed-fields?page=3"),
     *              @OA\Property(property="path",type="string",example="/api/v1/processed-fields"),
     *              @OA\Property(property="per_page",type="integer",example=25),
     *              @OA\Property(property="prev_page_url",type="string",example="/api/v1/processed-fields?page=1"),
     *              @OA\Property(property="to",type="integer",example=9),
     *              @OA\Property(property="total",type="integer",example=9)
     *          ),
     *          @OA\Property(property="errors",type="array",@OA\Items())
     *       )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Processed fields not found"
     *     ),
     *     @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *     ),
     *     security={
     *         {"Authentication": {}}
     *     }
     * )
     */
    public function report(Request $request)
    {
        $errors = [];
        $success = true;
        $object = null;

        $data = $request->all();
        $processed_fields = Helper::process_fields_report($data);

        return response()->json(["success" => $success, "objects" => $processed_fields, "errors" => $errors], Response::HTTP_OK);
    }

    /**
     * @OA\Delete(
     *   path="/api/v1/processed-field/delete/{id}",
     *   tags={"Processed Fields"},
     *   summary="Delete processed field in the site with id",
     *   description="",
     *   operationId="processedFieldDelete",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Processed field id to delete",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *       response="200",
     *       description="Returns the deletion success or errors.",
     *       @OA\JsonContent(
     *          @OA\Property(property="success",type="boolean",example=true),
     *          @OA\Property(property="errors",type="array",@OA\Items())
     *       )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Processed fields not found"
     *     ),
     *     @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *     ),
     *     security={
     *         {"Authentication": {}}
     *     }
     * )
     */
    public function delete(Request $request, $id)
    {
        $errors = [];
        $success = true;

        $field = ProcessedField::where('id', $id)->first();
        if ($field) {
            $field->delete();
            $success = true;
        } else {
            $success = false;
            $errors[] = "Invalid processed field.";
        }

        return response()->json(["success" => $success, "errors" => $errors], Response::HTTP_OK);
    }
}
