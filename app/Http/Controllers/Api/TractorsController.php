<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Tractor;
use App\Helpers\Helper;

class TractorsController extends Controller
{

    /**
     * @OA\Post(
     *   path="/api/v1/tractor/create",
     *   tags={"Tractors"},
     *   summary="Create a tractor in the site with form data",
     *   description="",
     *   operationId="tractorCreate",
     *   @OA\RequestBody(
     *       required=false,
     *       @OA\MediaType(
     *           mediaType="application/x-www-form-urlencoded",
     *           @OA\Schema(
     *               type="object",
     *               required={"name"},
     *               @OA\Property(
     *                   property="name",
     *                   description="The name for the tractor",
     *                   type="string"
     *               ),
     *               @OA\Property(
     *                   property="active",
     *                   description="Status of the crop type",
     *                   type="enum",
     *                   enum={0, 1}
     *               ),
     *           )
     *       )
     *   ),
     *   @OA\Response(
     *       response="200",
     *       description="Returns the created tractor object with the status or errors.",
     *       @OA\JsonContent(ref="#/components/schemas/Tractor")
     *   ),
     *   @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *   ),
     *   security={
     *         {"Authentication": {}}
     *   }
     * )
     */
    public function create(Request $request)
    {
        $errors = [];
        $success = false;
        $object = null;

        $data = $request->all();
        if (empty($data['name'])) {
            $errors[] = 'Please enter the tractor name.';
        }

        if (count($errors) == 0) {
            if ($object = Helper::createOrUpdateTractor($data, $errors)) {
                $success = true;
            }
        }

        return response()->json(["success" => $success, "object" => $object, "errors" => $errors], Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *   path="/api/v1/tractor/update",
     *   tags={"Tractors"},
     *   summary="Update a tractor in the site with form data",
     *   description="",
     *   operationId="tractorUpdate",
     *   @OA\RequestBody(
     *       required=false,
     *       @OA\MediaType(
     *           mediaType="application/x-www-form-urlencoded",
     *           @OA\Schema(
     *               type="object",
     *               required={"id"},
     *               @OA\Property(
     *                   property="id",
     *                   description="The id for the tractor",
     *                   type="integer",
     *                   minimum=1
     *               ),
     *               @OA\Property(
     *                   property="name",
     *                   description="The name for the tractor",
     *                   type="string"
     *               ),
     *               @OA\Property(
     *                   property="active",
     *                   description="Status of the crop type",
     *                   type="enum",
     *                   enum={0, 1}
     *               ),
     *           )
     *       )
     *   ),
     *   @OA\Response(
     *       response="200",
     *       description="Returns the created tractor object with the status or errors.",
     *       @OA\JsonContent(ref="#/components/schemas/Tractor")
     *   ),
     *   @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *   ),
     *   security={
     *         {"Authentication": {}}
     *   }
     * )
     */
    public function update(Request $request)
    {
        $errors = [];
        $success = false;
        $object = null;

        $data = $request->all();
        if (empty($data['id'])) {
            $errors[] = 'Please enter the tractor id.';
        }

        if (count($errors) == 0) {
            if ($object = Helper::createOrUpdateTractor($data, $errors)) {
                $success = true;
            }
        }

        return response()->json(["success" => $success, "object" => $object, "errors" => $errors], Response::HTTP_OK);
    }

    /**
     * @OA\Get(
     *   path="/api/v1/tractor/{id}",
     *   tags={"Tractors"},
     *   summary="Get a tractor in the site with parameters",
     *   description="",
     *   operationId="tractorRetrieve",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of tractor that needs to be fetched",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             minimum=1
     *         )
     *     ),
     *     @OA\Response(
     *       response="200",
     *       description="Returns the tractor object with the status or errors.",
     *       @OA\JsonContent(ref="#/components/schemas/Tractor")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Tractor not found"
     *     ),
     *     @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *     ),
     *     security={
     *         {"Authentication": {}}
     *     }
     * )
     */
    public function retrieve(Request $request, $id)
    {
        $errors = [];
        $success = false;
        $object = null;


        $object = Tractor::find($id);
        if ($object) {
            $success = true;
        } else {
            $errors[] = 'Invalid tractor id.';
        }

        return response()->json(["success" => $success, "object" => $object, "errors" => $errors], Response::HTTP_OK);
    }

    /**
     * @OA\Get(
     *   path="/api/v1/tractors",
     *   tags={"Tractors"},
     *   summary="Get tractors in the site with parameters",
     *   description="",
     *   operationId="tractorList",
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="Search the tractors by the name",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Current page",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Page limit (max=100)",
     *         @OA\Schema(type="integer",example=25,maximum=100)
     *     ),
     *     @OA\Response(
     *       response="200",
     *       description="Returns the tractor objects with the status or errors.",
     *       @OA\JsonContent(
     *          @OA\Property(property="success",type="boolean",example=true),
     *          @OA\Property(
     *               property="objects",
     *               type="object",
     *               @OA\Property(property="current_page",type="integer",example=1),
     *               @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                     type="object",
     *                     @OA\Property(property="id",type="integer",example=1),
     *                     @OA\Property(property="name",type="string",example="Test Tractor"),
     *                     @OA\Property(property="active",type="integer",example=0),
     *                     @OA\Property(property="created_by",type="integer",example=1),
     *                     @OA\Property(property="created_at",type="datetime",example="2019-08-24 15:23:16"),
     *                     @OA\Property(property="updated_at",type="datetime",example="2019-08-24 15:23:16")
     *                  )
     *              ),
     *              @OA\Property(property="first_page_url",type="string",example="/api/v1/tractors?page=1"),
     *              @OA\Property(property="from",type="integer",example=1),
     *              @OA\Property(property="last_page",type="integer",example=1),
     *              @OA\Property(property="last_page_url",type="string",example="/api/v1/tractors?page=2"),
     *              @OA\Property(property="next_page_url",type="string",example="/api/v1/tractors?page=3"),
     *              @OA\Property(property="path",type="string",example="/api/v1/tractors"),
     *              @OA\Property(property="per_page",type="integer",example=25),
     *              @OA\Property(property="prev_page_url",type="string",example="/api/v1/tractors?page=1"),
     *              @OA\Property(property="to",type="integer",example=9),
     *              @OA\Property(property="total",type="integer",example=9)
     *          ),
     *          @OA\Property(property="errors",type="array",@OA\Items())
     *       )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Tractors not found"
     *     ),
     *     @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *     ),
     *     security={
     *         {"Authentication": {}}
     *     }
     * )
     */
    public function list(Request $request)
    {
        $errors = [];
        $success = true;
        $object = null;

        $data = $request->all();

        $objects = Tractor::where('id', '>', 0);
        if (array_key_exists('name', $data) && !empty($data['name'])) {
            $objects = $objects->where('name', 'LIKE', "%".$data['name']."%");
        }
        $objects = Helper::api_filter($data, $objects);

        return response()->json(["success" => $success, "objects" => $objects, "errors" => $errors], Response::HTTP_OK);
    }
}
