<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Field;
use App\Helpers\Helper;

class FieldsController extends Controller
{

    /**
     * @OA\Post(
     *   path="/api/v1/field/create",
     *   tags={"Fields"},
     *   summary="Create a crop type in the site with form data",
     *   description="",
     *   operationId="fieldCreate",
     *   @OA\RequestBody(
     *       required=false,
     *       @OA\MediaType(
     *           mediaType="application/x-www-form-urlencoded",
     *           @OA\Schema(
     *               type="object",
     *               required={"name","crop_type_id","area"},
     *               @OA\Property(
     *                   property="name",
     *                   description="The name for the field",
     *                   type="string"
     *               ),
     *               @OA\Property(
     *                   property="crop_type_id",
     *                   description="Crop type id of the field",
     *                   type="integer",
     *                   minimum=1
     *               ),
     *               @OA\Property(
     *                   property="area",
     *                   description="Area of the field",
     *                   type="double"
     *               ),
     *               @OA\Property(
     *                   property="active",
     *                   description="Status of the crop type",
     *                   type="enum",
     *                   enum={0, 1}
     *               ),
     *           )
     *       )
     *   ),
     *   @OA\Response(
     *       response="200",
     *       description="Returns the created field object with the status or errors.",
     *       @OA\JsonContent(ref="#/components/schemas/Field")
     *   ),
     *   @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *   ),
     *   security={
     *         {"Authentication": {}}
     *   }
     * )
     */
    public function create(Request $request)
    {
        $errors = [];
        $success = false;
        $object = null;

        $data = $request->all();
        if (empty($data['name'])) {
            $errors[] = 'Please enter the field name.';
        }

        if (empty($data['crop_type_id'])) {
            $errors[] = 'Please enter id for the type of crops.';
        }

        if (empty($data['area'])) {
            $errors[] = 'Please enter the field area.';
        }
        if (!is_numeric($data['area'])) {
            $errors[] = 'Field area should be a numeric value.';
        }

        if (count($errors) == 0) {
            if ($object = Helper::createOrUpdateField($data, $errors)) {
                $success = true;
            }
        }

        return response()->json(["success" => $success, "object" => $object, "errors" => $errors], Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *   path="/api/v1/field/update",
     *   tags={"Fields"},
     *   summary="Update a crop type in the site with form data",
     *   description="",
     *   operationId="fieldUpdate",
     *   @OA\RequestBody(
     *       required=false,
     *       @OA\MediaType(
     *           mediaType="application/x-www-form-urlencoded",
     *           @OA\Schema(
     *               type="object",
     *               required={"id"},
     *               @OA\Property(
     *                   property="id",
     *                   description="The id for the field",
     *                   type="integer",
     *                   minimum=1
     *               ),
     *               @OA\Property(
     *                   property="name",
     *                   description="The name for the field",
     *                   type="string"
     *               ),
     *               @OA\Property(
     *                   property="crop_type_id",
     *                   description="Crop type id of the field",
     *                   type="integer",
     *                   minimum=1
     *               ),
     *               @OA\Property(
     *                   property="area",
     *                   description="Area of the field",
     *                   type="double"
     *               ),
     *               @OA\Property(
     *                   property="active",
     *                   description="Status of the crop type",
     *                   type="enum",
     *                   enum={0, 1}
     *               ),
     *           )
     *       )
     *   ),
     *   @OA\Response(
     *       response="200",
     *       description="Returns the created field object with the status or errors.",
     *       @OA\JsonContent(ref="#/components/schemas/Field")
     *   ),
     *   @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *   ),
     *   security={
     *         {"Authentication": {}}
     *   }
     * )
     */
    public function update(Request $request)
    {
        $errors = [];
        $success = false;
        $object = null;

        $data = $request->all();
        if (empty($data['id'])) {
            $errors[] = 'Please enter the field id.';
        }

        if (!empty($data['area'])) {
            if (!is_numeric($data['area'])) {
                $errors[] = 'Field area should be a numeric value.';
            }
        }

        if (count($errors) == 0) {
            if ($object = Helper::createOrUpdateField($data, $errors)) {
                $success = true;
            }
        }

        return response()->json(["success" => $success, "object" => $object, "errors" => $errors], Response::HTTP_OK);
    }

    /**
     * @OA\Get(
     *   path="/api/v1/field/{id}",
     *   tags={"Fields"},
     *   summary="Get a field in the site with parameters",
     *   description="",
     *   operationId="fieldRetrieve",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of field that needs to be fetched",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             minimum=1
     *         )
     *     ),
     *     @OA\Response(
     *       response="200",
     *       description="Returns the crop object with the status or errors.",
     *       @OA\JsonContent(ref="#/components/schemas/Field")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Field not found"
     *     ),
     *     @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *     ),
     *     security={
     *         {"Authentication": {}}
     *     }
     * )
     */
    public function retrieve(Request $request, $id)
    {
        $errors = [];
        $success = false;
        $object = null;


        $object = Field::find($id);
        if ($object) {
            $success = true;
        } else {
            $errors[] = 'Invalid field id.';
        }

        return response()->json(["success" => $success, "object" => $object, "errors" => $errors], Response::HTTP_OK);
    }

    /**
     * @OA\Get(
     *   path="/api/v1/fields",
     *   tags={"Fields"},
     *   summary="Get fields in the site with parameters",
     *   description="",
     *   operationId="fieldList",
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="Search the fields by the name",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="crop_type_id",
     *         in="query",
     *         description="Search the fields by the crop type",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Current page",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Page limit (max=100)",
     *         @OA\Schema(type="integer",example=25,maximum=100)
     *     ),
     *     @OA\Response(
     *       response="200",
     *       description="Returns the field objects with the status or errors.",
     *       @OA\JsonContent(
     *          @OA\Property(property="success",type="boolean",example=true),
     *          @OA\Property(
     *               property="objects",
     *               type="object",
     *               @OA\Property(property="current_page",type="integer",example=1),
     *               @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                     type="object",
     *                     @OA\Property(property="id",type="integer",example=1),
     *                     @OA\Property(property="name",type="string",example="Test Field"),
     *                     @OA\Property(property="crop_type_id",type="integer",example=1),
     *                     @OA\Property(property="area",type="double",example=120),
     *                     @OA\Property(property="active",type="integer",example=0),
     *                     @OA\Property(property="created_by",type="integer",example=1),
     *                     @OA\Property(property="created_at",type="datetime",example="2019-08-24 15:23:16"),
     *                     @OA\Property(property="updated_at",type="datetime",example="2019-08-24 15:23:16")
     *                  )
     *              ),
     *              @OA\Property(property="first_page_url",type="string",example="/api/v1/fields?page=1"),
     *              @OA\Property(property="from",type="integer",example=1),
     *              @OA\Property(property="last_page",type="integer",example=1),
     *              @OA\Property(property="last_page_url",type="string",example="/api/v1/fields?page=2"),
     *              @OA\Property(property="next_page_url",type="string",example="/api/v1/fields?page=3"),
     *              @OA\Property(property="path",type="string",example="/api/v1/fields"),
     *              @OA\Property(property="per_page",type="integer",example=25),
     *              @OA\Property(property="prev_page_url",type="string",example="/api/v1/fields?page=1"),
     *              @OA\Property(property="to",type="integer",example=9),
     *              @OA\Property(property="total",type="integer",example=9)
     *          ),
     *          @OA\Property(property="errors",type="array",@OA\Items())
     *       )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Fields not found"
     *     ),
     *     @OA\Response(
     *       response="401",
     *       description="Unauthorized"
     *     ),
     *     security={
     *         {"Authentication": {}}
     *     }
     * )
     */
    public function list(Request $request)
    {
        $errors = [];
        $success = true;
        $object = null;

        $data = $request->all();

        $objects = Field::where('id', '>', 0);
        if (array_key_exists('name', $data) && !empty($data['name'])) {
            $objects = $objects->where('name', 'LIKE', "%".$data['name']."%");
        }
        if (array_key_exists('crop_type_id', $data) && !empty($data['crop_type_id'])) {
            $objects = $objects->where('crop_type_id', $data['crop_type_id']);
        }
        $objects = Helper::api_filter($data, $objects);

        return response()->json(["success" => $success, "objects" => $objects, "errors" => $errors], Response::HTTP_OK);
    }
}
