<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\User;

class ApiTokenController extends Controller
{

    /**
     * @OA\SecurityScheme(
     *     securityScheme="Authentication",
     *     in="header",
     *     type="http",
     *     description="Use the token you get from the login api.",
     *     name="Authentication",
     *     scheme="bearer",
     *     bearerFormat="bearer",
     * )
     */

    /**
     * @OA\Post(
     *   path="/api/v1/login",
     *   tags={"Authentication"},
     *   summary="Login with email and password to get the api token.",
     *   description="",
     *   operationId="login",
     *   @OA\RequestBody(
     *       required=false,
     *       @OA\MediaType(
     *           mediaType="application/x-www-form-urlencoded",
     *           @OA\Schema(
     *               type="object",
     *               required={"email","password"},
     *               @OA\Property(
     *                   property="email",
     *                   description="Your account email",
     *                   type="string"
     *               ),
     *               @OA\Property(
     *                   property="password",
     *                   description="Your account password",
     *                   type="password"
     *               ),
     *           )
     *       )
     *   ),
     *   @OA\Response(
     *       response="200",
     *       description="Returns the api token and the token expired date & time with the status or errors.",
     *       @OA\JsonContent(ref="#/components/schemas/User"),
     *   ),
     * )
     */
    public function update(Request $request)
    {
        $errors = [];
        $data = $request->all();
        if (empty($data['email'])) {
            $errors[] = 'Please enter the email.';
        }

        if (empty($data['password'])) {
            $errors[] = 'Please enter the password.';
        }

        $token = null;
        $expired = null;
        if (count($errors) === 0) {
            $user = User::where('email', $data['email'])->where('active', true)->first();
            if ($user) {
                if(Auth::guard('web')->attempt(['email' => $data['email'], 'password' => $data['password']])){
                    $token = Str::random(60);
                    $user->api_token = hash('sha256', $token);
                    $expired = date('Y-m-d H:i:s', strtotime('+1 hours'));
                    $user->api_expired = $expired;
                    $expired = strtotime($expired);
                    $user->save();
                } else {
                    $errors[] = 'Password is not correct, please try again.';
                }
            } else {
                $errors[] = 'System cannot find the email address, please try again.';
            }
        }

        return response()->json([ "token" => $token, "expired" => $expired, "errors" => $errors], Response::HTTP_OK);
    }
}