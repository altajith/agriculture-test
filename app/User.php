<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * @OA\Schema(@OA\Xml(name="User"))
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * @OA\Property(property="token",type="string",example="9dmuIYeI5YBJy5of7sANuZ5gpQ9onjzFDXFsWeKLy509M5b7NyF2wWzuHnyf")
     * @var string
     */
    /**
     * @OA\Property(property="expired",type="integer",example=1566663719)
     * @var integer
     */
    /**
     * @OA\Property(property="errors",type="array",@OA\Items())
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the user record associated
     *
     * @return User
     */
    public function created_user()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
