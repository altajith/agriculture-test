<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use App\Models\CropType;
use App\Models\Field;
use App\Models\Tractor;
use App\Models\ProcessedField;

class Helper
{
    public static function success(&$request)
    {
        $success = $request->session()->get('success');
        $request->session()->put('success', null);
        return $success;
    }

    public static function errors(&$request)
    {
        $errors = $request->session()->get('errors');
        $request->session()->put('errors', []);
        return $errors;
    }

    public static function redirect($route, &$request, $data = [], $errors = [])
    {
        $request->session()->put('errors', $errors);
        if (array_key_exists('id', $data) && $data['id'] > 0) {
            return redirect()->route($route, ['id' => $data['id']]);
        } else {
            return redirect()->route($route);
        }
    }

    public static function web_user() {
        return Auth::guard('web')->user();
    }

    public static function createOrUpdateCropType($data, &$errors) {
        $user = self::web_user();
        if ($user) {
            if (array_key_exists('id', $data) && $data['id'] > 0) {
                if(CropType::where('id', '!=', $data['id'])->where('type_name', $data['type_name'])->exists()) {
                    $errors[] = $data['type_name'].' is already in the system, please enter a different type name.';
                }
                if (count($errors) > 0) return false;
                $object = CropType::find($data['id']);
                if (!$object) return false;
            } else {
                if(CropType::where('type_name', $data['type_name'])->exists()) {
                    $errors[] = $data['type_name'].' is already in the system, please enter a different type name.';
                }
                if (count($errors) > 0) return false;
                $object = new CropType();
                $object->created_by = $user->id;
            }

            if (array_key_exists('type_name', $data) && !empty($data['type_name'])) {
                $object->type_name = $data['type_name'];
            }
            if (array_key_exists('active', $data) && $data['active'] != '') {
                $object->active = !empty($data['active']) && $data['active'] == 1 ? 1:0;
            }
            try {
                if ($object->save()) {
                    return $object;
                }
            } catch(\Exception $e) {
                $errors[] = 'Invalid data detected.';
            }
        }
        return false;
    }

    public static function createOrUpdateField($data, &$errors) {
        $user = self::web_user();
        if ($user) {
            if (array_key_exists('crop_type_id', $data) && !empty($data['crop_type_id'])) {
                if (!CropType::where('id', $data['crop_type_id'])->where('active', true)->exists()) {
                    $errors[] = "Crop type is not valid.";
                }
            }
            if (count($errors) > 0) return false;

            if (array_key_exists('id', $data) && $data['id'] > 0) {
                $object = Field::find($data['id']);
                if (!$object) return false;
            } else {
                $object = new Field();
                $object->created_by = $user->id;
            }
            if (array_key_exists('name', $data) && !empty($data['name'])) {
                $object->name = $data['name'];
            }
            if (array_key_exists('area', $data) && !empty($data['area'])) {
                $object->area = $data['area'];
            }
            if (array_key_exists('crop_type_id', $data) && !empty($data['crop_type_id'])) {
                $object->crop_type_id = $data['crop_type_id'];
            }
            if (array_key_exists('active', $data) && $data['active'] != '') {
                $object->active = !empty($data['active']) && $data['active'] == 1 ? 1:0;
            }
            try {
                if ($object->save()) {
                    return $object;
                }
            } catch(\Exception $e) {
                $errors[] = 'Invalid data detected.';
            }
        }
        return false;
    }

    public static function createOrUpdateTractor($data, &$errors) {
        $user = self::web_user();
        if ($user) {
            if (array_key_exists('id', $data) && $data['id'] > 0) {
                if(Tractor::where('id', '!=', $data['id'])->where('name', $data['name'])->exists()) {
                    $errors[] = $data['name'].' is already in the system, please enter a different tractor name.';
                }
                if (count($errors) > 0) return false;
                $object = Tractor::find($data['id']);
                if (!$object) return false;
            } else {
                if(Tractor::where('name', $data['name'])->exists()) {
                    $errors[] = $data['name'].' is already in the system, please enter a different tractor name.';
                }
                if (count($errors) > 0) return false;
                $object = new Tractor();
                $object->created_by = $user->id;
            }

            if (array_key_exists('name', $data) && !empty($data['name'])) {
                $object->name = $data['name'];
            }
            if (array_key_exists('active', $data) && $data['active'] != '') {
                $object->active = !empty($data['active']) && $data['active'] == 1 ? 1:0;
            }

            try {
                if ($object->save()) {
                    return $object;
                }
            } catch(\Exception $e) {
                $errors[] = 'Invalid data detected.';
            }
        }
        return false;
    }

    public static function createOrUpdateProcessedField($data, &$errors) {
        $user = self::web_user();
        if ($user) {
            if (array_key_exists('field_id', $data) && !empty($data['field_id'])) {
                if (!Field::where('id', $data['field_id'])->where('active', true)->exists()) {
                    $errors[] = 'Field is not valid.';
                }
            }
            if (array_key_exists('tractor_id', $data) && !empty($data['tractor_id'])) {
                if (!Tractor::where('id', $data['tractor_id'])->where('active', true)->exists()) {
                    $errors[] = 'Tractor is not valid.';
                }
            }
            if (count($errors) > 0) return false;
            if (array_key_exists('id', $data) && $data['id'] > 0) {
                $object = ProcessedField::find($data['id']);
                if (!$object) return false;
            } else {
                $object = new ProcessedField();
                $object->created_by = $user->id;
                $object->status = 'pending';
            }
            if (array_key_exists('field_id', $data) && !empty($data['field_id'])) {
                $object->field_id = $data['field_id'];
            }
            if (array_key_exists('tractor_id', $data) && !empty($data['tractor_id'])) {
                $object->tractor_id = $data['tractor_id'];
            }
            if (array_key_exists('status', $data)) {
                if ($user->type == 'admin' && !empty($data['id']) && $data['id'] > 0 && !empty($data['status'])) {
                    $object->status = $data['status'];
                }
            }
            if (array_key_exists('the_date', $data) && !empty($data['the_date'])) {
                $object->the_date = $data['the_date'];
            }
            if (array_key_exists('area_limit', $data) && !empty($data['area_limit'])) {

                if (array_key_exists('id', $data) && $data['id'] > 0) {
                    $field = Field::find($object->field_id);
                    if ($field) {
                        if ($data['area_limit'] > $field->area) {
                            $errors[] = 'Processed area cannot be exceeded '.$field->area.'.';
                            if (count($errors) > 0) return false;
                        }
                    }
                }
                $object->area_limit = $data['area_limit'];
            }

            try {
                if ($object->save()) {
                    return $object;
                }
            } catch(\Exception $e) {
                $errors[] = 'Invalid data detected.';
            }
        }
        return false;
    }

    public static function api_filter($data, $objects) {
        $limit = 25;
        if (array_key_exists('limit', $data) && !empty($data['limit']) && is_numeric($data['limit'])) {
            if ($data['limit'] <= 100) {
                $limit = $data['limit'];
            }
        }
        $objects = $objects->paginate($limit);

        return $objects;
    }

    public static function process_fields_report($data) {

        $limit = 25;
        if (array_key_exists('limit', $data) && !empty($data['limit']) && is_numeric($data['limit'])) {
            if ($data['limit'] <= 100) {
                $limit = $data['limit'];
            }
        }

        $processed_fields = ProcessedField::where('status', 'approved');

        if (!empty($data['field'])) {
            $field = $data['field'];
            $processed_fields = $processed_fields->whereHas('field', function($q) use ($field) {
                $q->where('name', 'LIKE', "%$field%");
            });
        }

        if (!empty($data['tractor'])) {
            $tractor = $data['tractor'];
            $processed_fields = $processed_fields->whereHas('tractor', function($q) use ($tractor) {
                $q->where('name', 'LIKE', "%$tractor%");
            });
        }

        if (!empty($data['date'])) {
            $processed_fields = $processed_fields->where('the_date', $data['date']);
        }

        if (!empty($data['culture'])) {
            $culture = $data['culture'];
            $processed_fields = $processed_fields->whereHas('field', function($q) use ($culture) {
                $q->whereHas('type', function($q1) use ($culture) {
                    $q1->where('type_name', 'LIKE', "%$culture%");
                });
            });
        }

        $processed_fields = $processed_fields->orderBy('id', 'asc')->paginate($limit);

        return $processed_fields;
    }
}
