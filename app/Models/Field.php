<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(@OA\Xml(name="Field"))
 */
class Field extends Model
{
    /**
     * @OA\Property(property="success",type="boolean",example=true)
     */
    /**
     * @OA\Property(
     *      property="object",
     *      type="object",
     *      @OA\Property(property="id",type="integer",example=1),
     *      @OA\Property(property="name",type="string",example="Test Field"),
     *      @OA\Property(property="crop_type_id",type="integer",example=1),
     *      @OA\Property(property="area",type="double",example=120),
     *      @OA\Property(property="active",type="integer",example=0),
     *      @OA\Property(property="created_by",type="integer",example=1),
     *      @OA\Property(property="created_at",type="datetime",example="2019-08-24 15:23:16"),
     *      @OA\Property(property="updated_at",type="datetime",example="2019-08-24 15:23:16"),
     *  )
     */
    /**
     * @OA\Property(property="errors",type="array",@OA\Items())
     */

    protected $table = "fields";

    /**
     * Get the crop type record associated with the field.
     *
     * @return CropType
     */
    public function type()
    {
        return $this->hasOne(CropType::class, 'id', 'crop_type_id');
    }

    /**
     * Get the user record associated
     *
     * @return User
     */
    public function user()
    {
        return $this->hasOne(\App\User::class, 'id', 'created_by');
    }
}