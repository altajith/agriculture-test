<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(@OA\Xml(name="ProcessedField"))
 */
class ProcessedField extends Model
{

    /**
     * @OA\Property(property="success",type="boolean",example=true)
     */

    /**
     * @OA\Property(
     *      property="object",
     *      type="object",
     *      @OA\Property(property="id",type="integer",example=1),
     *      @OA\Property(property="tractor_id",type="integer",example=1),
     *      @OA\Property(property="field_id",type="integer",example=1),
     *      @OA\Property(property="the_date",type="date",example="2019-10-20"),
     *      @OA\Property(property="area_limit",type="double",example=100),
     *      @OA\Property(property="status",type="string",example="pending"),
     *      @OA\Property(property="created_by",type="integer",example=1),
     *      @OA\Property(property="created_at",type="datetime",example="2019-08-24 15:23:16"),
     *      @OA\Property(property="updated_at",type="datetime",example="2019-08-24 15:23:16"),
     *  )
     */
    /**
     * @OA\Property(property="errors",type="array",@OA\Items())
     */

    protected $table = "processed_fields";

    /**
     * Get the field record associated with the processed field.
     *
     * @return Field
     */
    public function field()
    {
        return $this->hasOne(Field::class, 'id', 'field_id');
    }

    /**
     * Get the tractor record associated with the processed field.
     *
     * @return Tractor
     */
    public function tractor()
    {
        return $this->hasOne(Tractor::class, 'id', 'tractor_id');
    }

    /**
     * Get the user record associated
     *
     * @return User
     */
    public function user()
    {
        return $this->hasOne(\App\User::class, 'id', 'created_by');
    }
}